# local_manifests (Build Pixel Experience!)

## Getting started (Syncing the treees)

```
cd ~/android/system or where your ROM source root will be.
repo init -u https://github.com/PixelExperience/manifest.git -b thirteen
git clone https://gitlab.com/expressluke-982x-t/pixelexperience/local_manifests.git .repo/local_manifeste
repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags
```

## Set up  build environment
$ . build/envsetup.sh

# Choose a target
$ lunch aosp_$device-userdebug

beyond0lte = Galaxy S10e
beyond1lte = Galaxy S10
beyond2lte = Galaxy S10+
beyondx = Galaxy S10 5G
d1 = Galaxy Note 10
d2s = Galaxy Note 10+
d2x = Galaxy Note 10+ 5G

# Build the rom
$ mka bacon -j$(nproc --all)